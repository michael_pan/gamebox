package com.gamebox.games;

import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicInteger;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.gamebox.R;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

public class GamesDBHelper extends OrmLiteSqliteOpenHelper {

	private static final String TAG = GamesDBHelper.class.getSimpleName();
	private static final String DATABASE_NAME = "games.db";
	private static final int DATABASE_VERSION = 6;

	// the DAO object we use to access the ResGame table
	private Dao<ResGame, Integer> gamesDao;
	private static final AtomicInteger usageCounter = new AtomicInteger(0);

	// we do this so there is only one helper
	private static GamesDBHelper gamesDBHelper = null;

	// Context 传入Application Context
	public static synchronized GamesDBHelper getHelper(Context ctx) {
		// 如果context为空
		if (ctx == null) {
			throw new NullPointerException("Context cannot be null");
		}
		
		if (gamesDBHelper == null) {
			gamesDBHelper = new GamesDBHelper(ctx);
		}
		usageCounter.incrementAndGet();
		return gamesDBHelper;
	}

	public GamesDBHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION,
				R.raw.games_config);
	}

	@Override
	public void onCreate(SQLiteDatabase sqliteDatabase,
			ConnectionSource connectionSource) {
		try {
			TableUtils.createTable(connectionSource, ResGame.class);
		} catch (SQLException e) {
			Log.e(TAG, "Unable to create datbases", e);
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase sqliteDatabase,
			ConnectionSource connectionSource, int oldVer, int newVer) {
		try {
			TableUtils.dropTable(connectionSource, ResGame.class, true);
			onCreate(sqliteDatabase, connectionSource);
		} catch (SQLException e) {
			Log.e(TAG, "Unable to upgrade database from version " + oldVer
					+ " to new " + newVer, e);
		}
	}

	@Override
	public void close() {
		if (usageCounter.decrementAndGet() == 0) {
			super.close();
			gamesDao = null;
			gamesDBHelper = null;
		}
	}

	public Dao<ResGame, Integer> getGamesDao() throws SQLException {
		if (gamesDao == null) {
			gamesDao = getDao(ResGame.class);
		}
		return gamesDao;
	}

}
