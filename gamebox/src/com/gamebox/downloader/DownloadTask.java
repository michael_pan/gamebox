package com.gamebox.downloader;

import java.io.Serializable;

import android.text.TextUtils;
import android.webkit.URLUtil;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "downloadtask")
public class DownloadTask implements Serializable {

	private static final long serialVersionUID = -6799192200096505631L;

	@DatabaseField(generatedId = true, canBeNull = false)
	public int id = -1;

	@DatabaseField(canBeNull = false)
	public String name = "";

	@DatabaseField(canBeNull = false)
	public String mimeType = "";

	@DatabaseField(canBeNull = false)
	public String title = "";

	@DatabaseField(canBeNull = false)
	public String desc = "";

	@DatabaseField(canBeNull = false)
	public String savepath = "";

	@DatabaseField(canBeNull = false)
	public int status = DownloadStatus.PENDING;

	@DatabaseField(canBeNull = false)
	public long totalsize = 0;

	@DatabaseField(canBeNull = false)
	public long currentsize = 0;

	@DatabaseField(canBeNull = false)
	public String url = "";

	@Override
	public String toString() {
		return id + "," + mimeType + "," + title + "," + desc + "," + name
				+ "," + savepath + "," + status + "," + totalsize + "," + url;
	}

	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}

	@Override
	public int hashCode() {
		return url.hashCode();
	}

	public boolean isValid() {
		return !TextUtils.isEmpty(url) && URLUtil.isNetworkUrl(url);
	}

	public boolean isCanneled() {
		return status == DownloadStatus.CANCELED;
	}

	public boolean isPause() {
		return status == DownloadStatus.PAUSE;
	}
}
