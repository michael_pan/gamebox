package com.gamebox.cache;

import java.io.File;
import java.io.InputStream;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.support.v4.util.LruCache;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageView;

//功能：定义统一的图片缓存入口，管理所有的图片缓存
//1.硬盘缓存
//2.内存缓存
//3.从硬盘和网络加载图片时，图片动画渐现（参考：http://www.oschina.net/code/snippet_219356_18887）
//4. 解决OOM的问题 具体方案(https://github.com/yueyueniao2012/multiimagechooser)
//HandlerThread+LruCache(Memory Cache)+DiskLruCache(FileCache)

public class PicMgr {
	private static final String TAG = PicMgr.class.getSimpleName();
	// 内存Cache,用于 URL和Cache的对应Map
	public LruCache<String, Drawable> bitmapCache;
	// 用于存放ImageView和URL对应的Map
	private static Map<ImageView, String> imageViews;
	// HandleThread and Handler
	private Handler mLoaderHandler;
	private HandlerThread mLoaderThread;

	// 默认显示图片
	private int defImgID = 0;
	private static PicMgr sInstance = null;
	// Debug标识
	private static boolean isDebug = false;
	// Context
	private static Context mCtx = null;

	// 文件缓存的定义
	private static final int DISK_CACHE_SIZE = 1024 * 1024 * 10; // 10MB
	private static final String DISK_CACHE_SUBDIR = "thumbnails";
	public DiskLruCache mDiskCache;

	public static PicMgr getInstance(Context ctx) {
		if (sInstance == null) {
			synchronized (PicMgr.class) {
				if (sInstance == null) {
					sInstance = new PicMgr(ctx);
				}
			}
		}
		return sInstance;
	}

	private PicMgr(Context ctx) {
		// 如果不在ui线程中，则抛出异常
		if (Looper.myLooper() != Looper.getMainLooper()) {
			throw new RuntimeException("Cannot instantiate outside UI thread.");
		}

		// 如果context为空
		if (ctx == null) {
			throw new NullPointerException("Context cannot be null");
		}

		mCtx = ctx;
		// 内存缓存
		int memClass = ((ActivityManager) mCtx
				.getSystemService(Context.ACTIVITY_SERVICE)).getMemoryClass();
		memClass = memClass > 32 ? 32 : memClass;
		// 使用可用内存的1/8作为图片缓存
		final int cacheSize = 1024 * 1024 * memClass / 8;
		if (isDebug) {
			Log.i(TAG, "cacheSize = " + cacheSize);
		}
		bitmapCache = new LruCache<String, Drawable>(cacheSize) {

			protected int sizeOf(String key, Drawable drawable) {
				Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
				if (isDebug) {
					Log.i(TAG, "LruCache bitmap size = " + bitmap.getRowBytes()
							* bitmap.getHeight());
				}
				return bitmap.getRowBytes() * bitmap.getHeight();
			}
		};

		// 存放Imageview和URL对应关系
		imageViews = Collections
				.synchronizedMap(new WeakHashMap<ImageView, String>());
		// 文件缓存
		File cacheDir = DiskLruCache.getDiskCacheDir(mCtx, DISK_CACHE_SUBDIR);

		if (isDebug) {
			Log.i(TAG, "cacheDir = " + cacheDir.getAbsolutePath());
		}

		mDiskCache = DiskLruCache.openCache(mCtx, cacheDir, DISK_CACHE_SIZE);

		if (isDebug) {
			Log.i(TAG, "mDiskCache = " + mDiskCache);
		}
		mDiskCache.setCompressParams(CompressFormat.PNG, 85);

		mLoaderThread = new HandlerThread("image-load",
				Process.THREAD_PRIORITY_MORE_FAVORABLE);
		mLoaderThread.start();
		mLoaderHandler = new Handler(mLoaderThread.getLooper());
	}

	// 设置默认图片
	public void setDefaultImgId(int id) {
		defImgID = id;
	}

	// 加载图片,显示默认图片
	public void loadBitmap(ImageView iv, String url) {
		loadBitmap(iv, url, defImgID);
	}

	// 加载图片,可设置默认图片
	public void loadBitmap(final ImageView iv, final String url, final int defID) {
		// 如果不在ui线程中，则抛出异常
		if (Looper.myLooper() != Looper.getMainLooper()) {
			throw new RuntimeException("Cannot instantiate outside UI thread.");
		}

		Drawable drawable = null;
		if (TextUtils.isEmpty(url)) {
			if (isDebug) {
				Log.w(TAG, "url is empty!!!!");
			}
			setImage(iv, drawable, defID);
			return;
		}

		// if (isDebug) {
		// Log.i(TAG, "loadBitmap iv = " + iv + " , url = " + url
		// + ", defID = " + defID);
		// }
		imageViews.put(iv, url);

		ImageCallback cb = new ImageCallback() {

			@Override
			public void onLoad(Drawable drawable, String url) {
				if (drawable == null)
					return;
				String oldurl = imageViews.get(iv);
				if (!TextUtils.isEmpty(oldurl) && oldurl.equals(url)) {
					setImageAni(iv, drawable, defID, true);
				}
			}
		};
		// 异步加载图片
		drawable = load(url, cb);
		// 设置图片
		setImage(iv, drawable, defID);
	}

	// 清理执行的异步runnable异步任务
	public void removeAllTasks() {
		if (mLoaderHandler != null) {
			mLoaderHandler.removeCallbacksAndMessages(null);
			imageViews.clear();
		}
	}

	// 定义图片加载回调接口
	private interface ImageCallback {
		public void onLoad(Drawable drawable, String url);
	}

	// 加载的具体操作放这里
	private Drawable load(final String url, final ImageCallback callback) {
		if (url == null) {
			return null;
		}
		// 从缓存中取
		Drawable drawable = bitmapCache.get(url);
		if (drawable != null) {
			if (isDebug) {
				// Log.i(TAG, "drawable is in cache" + " ,url = " + url);
				Log.i(TAG, "bitmapCache = " + bitmapCache);
			}
			return drawable;
		}
		final Handler handler = new Handler() {
			@Override
			public void handleMessage(Message message) {
				callback.onLoad((Drawable) message.obj, url);
			}
		};

		mLoaderHandler.post(new Runnable() {

			@Override
			public void run() {
				if (!imageViews.containsValue(url)) {
					if (isDebug) {
						Log.w(TAG, "url is outdate!!!!" + " ,url = " + url);
					}
					return;
				}
				// 从文件缓存读取
				Drawable drawable = null;
				final Bitmap bm = mDiskCache.get(url);
				if (bm != null) {
					drawable = new BitmapDrawable(mCtx.getResources(), bm);
				}
				long t1 = System.currentTimeMillis();
				if (drawable == null) {
					// 文件缓存没有，从网络获取
					drawable = getBitmapFromNet(url);
					if (drawable != null) {
						mDiskCache.put(url,
								((BitmapDrawable) drawable).getBitmap());
					}
				}
				long t = System.currentTimeMillis() - t1;
				if (isDebug) {
					Log.i(TAG, "dt = " + t + "ms ,url = " + url);
				}
				if (drawable != null) {
					bitmapCache.put(url, drawable);
					Message msg = Message.obtain();
					msg.obj = drawable;
					handler.sendMessage(msg);
				}
			}
		});

		return drawable;
	}

	// 不带渐现动画的Imageview
	private void setImage(final ImageView iv, final Drawable drawable,
			final int defID) {
		setImageAni(iv, drawable, defID, false);
	}

	// 带渐现的动画的Imageview的显示
	private void setImageAni(final ImageView iv, final Drawable drawable,
			final int defID, final boolean withAnimation) {

		// if (isDebug) {
		// Log.i(TAG, "setImage iv = " + iv + " , drawable = " + drawable
		// + ", defID = " + defID + " ,withAnimation = "
		// + withAnimation);
		// }

		// 如果是空，显示默认图片,设置背景色
		if (drawable == null && defID != 0) {
			iv.setImageResource(defID);
		} else if (drawable != null) {
			// with animation
			if (withAnimation) {
				final TransitionDrawable td = new TransitionDrawable(
						new Drawable[] {
								new ColorDrawable(android.R.color.transparent),
								drawable });
				td.setCrossFadeEnabled(true);
				iv.setImageDrawable(td);
				td.startTransition(300);
			} else {
				// without animation
				iv.setImageDrawable(drawable);
			}
		} else {
			iv.setImageDrawable(null);
		}
	}

	// // 从文件获取Bitmap
	// private static Drawable getBitmapFromFile(String url) {
	// final String imagePath = FileUtils.getPicPathFromURL(url);
	// return getBitmapFromCachePath(imagePath);
	// }

	// 从网络获取图片,并且保存到文件,做文件缓存
	private static Drawable getBitmapFromNet(String url) {
		// final String imagePath = FileUtils.getPicPathFromURL(url);
		// long t1 = System.currentTimeMillis();
		Bitmap bm = get(url);
		// long t2 = System.currentTimeMillis();
		Drawable drawable = null;
		if (bm != null) {
			drawable = new BitmapDrawable(mCtx.getResources(), bm);
			// FileUtils.Bitmap2PNG(bm, imagePath);
		}
		// long t3 = System.currentTimeMillis();
		// Log.i(TAG, "(t2 - t1) = "+(t2 - t1)+"  , (t3-t2) = "+(t3-t2));
		return drawable;
	}

	// 下载网络图片
	private static Bitmap get(String url) {
		if (url == null) {
			return null;
		}
		Bitmap bm = null;
		InputStream is = null;
		try {
			is = new DefaultHttpClient().execute(new HttpGet(url)).getEntity()
					.getContent();
			bm = BitmapFactory.decodeStream(is);
		} catch (Exception e) {
			// e.printStackTrace();
		} finally {
			try {
				if (is != null)
					is.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return bm;
	}

	// // 从文件中解码Bitmap
	// private static Drawable getBitmapFromCachePath(String imagePath) {
	// Drawable drawable = null;
	// File file = new File(imagePath);
	// if (file.exists() && file.isFile()) {
	// drawable = Drawable.createFromPath(file.getPath());
	// if (drawable == null) {
	// file.delete();
	// } else {
	// // Log.e(TAG, "getBitmapFromCachePath success!!!");
	// return drawable;
	// }
	// }
	// return drawable;
	// }

	public Bitmap getBitmapByUrl(String url) {
		return mDiskCache.get(url);
	}
}
