package com.gamebox.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class CommonPreferences {
	private static final String GAMEBOX_PRE = "clemu";

	private static final String OPEN_COUNT = "open_count";
	private static final String HAS_PAY = "has_pay";

	public static void setOpenCount(Context ctx, int sortType) {
		saveData(ctx, GAMEBOX_PRE, OPEN_COUNT, sortType);
	}

	public static int getOpenCount(Context ctx) {
		return (Integer) getData(ctx, GAMEBOX_PRE, OPEN_COUNT, 0);
	}

	public static void setHasPay(Context ctx, boolean hasRoot) {
		saveData(ctx, GAMEBOX_PRE, HAS_PAY, hasRoot);
	}

	public static boolean getHasPay(Context ctx) {
		return (Boolean) getData(ctx, GAMEBOX_PRE, HAS_PAY, false);
	}

	/**
	 * 保存数据
	 * 
	 * @param context
	 * @param fileName
	 * @param key
	 * @param value
	 */
	private static void saveData(Context ctx, String fileName, String key,
			Object value) {
		SharedPreferences sp = ctx.getSharedPreferences(fileName,
				Context.MODE_PRIVATE);
		Editor editor = sp.edit();
		if (value instanceof Boolean) {
			editor.putBoolean(key, (Boolean) value);
		} else if (value instanceof Integer) {
			editor.putInt(key, (Integer) value);
		} else if (value instanceof Long) {
			editor.putLong(key, (Long) value);
		} else if (value instanceof Float) {
			editor.putFloat(key, (Float) value);
		} else {
			if (value == null) {
				editor.putString(key, "");
			} else {
				editor.putString(key, String.valueOf(value));
			}
		}
		editor.commit();
	}

	/**
	 * 取值
	 * 
	 * @param context
	 * @param fileName
	 * @param key
	 * @param defValue
	 * @return
	 */
	private static Object getData(Context ctx, String fileName, String key,
			Object defValue) {
		SharedPreferences sp = ctx.getSharedPreferences(fileName,
				Context.MODE_PRIVATE);
		if (defValue instanceof Boolean) {
			return sp.getBoolean(key, (Boolean) defValue);
		} else if (defValue instanceof Integer) {
			return sp.getInt(key, (Integer) defValue);
		} else if (defValue instanceof Long) {
			return sp.getLong(key, (Long) defValue);
		} else if (defValue instanceof Float) {
			return sp.getFloat(key, (Float) defValue);
		} else {
			if (defValue == null) {
				return sp.getString(key, "");
			}
			return sp.getString(key, String.valueOf(defValue));
		}
	}
}
