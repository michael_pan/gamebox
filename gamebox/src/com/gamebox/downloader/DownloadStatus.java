package com.gamebox.downloader;

public class DownloadStatus {
	public static final int NONE = 0; // 表示没有该任务
	// task is pending
	public static final int PENDING = 1 << 0;
	// task is running
	public static final int RUNNING = 1 << 1;
	// task is pause
	public static final int PAUSE = 1 << 2;
	// task is canceled
	public static final int CANCELED = 1 << 3;
	// task is success
	public static final int SUCCESS = 1 << 4;
	// task is failed
	public static final int FAILED = 1 << 5;

}
