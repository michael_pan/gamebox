package com.gamebox.utils;

import java.io.File;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.gamebox.cache.GLOBAL;
import com.gamebox.cache.PicMgr;
import com.seleuco.mame4all.MAME4all;

public class UIUtils {
    public static void createShortcutIntent(Context ctx, String gamename, String romurl, String picurl) {
        try {			
            // Intent used when the icon is touched
            final Intent shortcutIntent = new Intent(ctx, MAME4all.class);
            shortcutIntent.setAction(Intent.ACTION_VIEW);
            shortcutIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            shortcutIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
			File path = new File(GLOBAL
					.getSavePathFromURL(romurl));
            shortcutIntent.setData(Uri.fromFile(path));

            // Intent that actually sets the shortcut
            final Intent intent = new Intent();
            intent.putExtra(Intent.EXTRA_SHORTCUT_ICON, PicMgr.getInstance(ctx).getBitmapByUrl(picurl));
            intent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
            intent.putExtra(Intent.EXTRA_SHORTCUT_NAME, gamename);
            intent.putExtra("duplicate", false);
            intent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
            ctx.sendBroadcast(intent);
        } catch (final Exception e) {
            Log.e("UIUtils", "createShortcutIntent", e);
        }
    }
}
