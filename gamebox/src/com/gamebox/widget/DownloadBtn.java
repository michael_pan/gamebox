package com.gamebox.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

import com.gamebox.R;

public class DownloadBtn extends Button {

	public DownloadBtn(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	public DownloadBtn(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public DownloadBtn(Context context) {
		super(context);
		init();
	}

	private void init() {
		setBackgroundResource(R.drawable.common_btn);
	}
	
	

}
