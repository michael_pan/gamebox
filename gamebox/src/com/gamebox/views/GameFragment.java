package com.gamebox.views;

import java.sql.SQLException;
import java.sql.Savepoint;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.gamebox.R;
import com.gamebox.adapter.GameListAdapter;
import com.gamebox.downloader.DownloadListener;
import com.gamebox.downloader.DownloadManager;
import com.gamebox.downloader.DownloadTask;
import com.gamebox.games.Games;
import com.gamebox.games.GamesDBHelper;
import com.gamebox.games.ResGame;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.DatabaseConnection;

public class GameFragment extends BaseFragment implements DownloadListener {

	private static GameFragment mGameFragment;

	private GamesDBHelper gamesDBHelper = null;

	private DownloadManager dm = null;

	public static GameFragment getInstance() {
		if (mGameFragment == null) {
			synchronized (GameFragment.class) {
				if (mGameFragment == null)
					mGameFragment = new GameFragment();
			}
		}
		return mGameFragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		gamesDBHelper = GamesDBHelper.getHelper(getActivity()
				.getApplicationContext());
		initDB();
		dm = DownloadManager.getInstance(getActivity().getApplicationContext());
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return initView(inflater, container, savedInstanceState);
	}

	@Override
	public void onResume() {
		super.onResume();
		if (dm != null)
			dm.registerDownloadListener(this);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (gamesDBHelper != null) {
			gamesDBHelper.close();
			gamesDBHelper = null;
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		menu.clear();
		MenuItem item;
//		item = menu.add(0, MENU_SEARCH, 0, R.string.menu_search);
//		item.setIcon(R.drawable.ic_action_search);
//		item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

		item = menu.add(0, MENU_SHARE, 0, R.string.menu_share);
		item.setIcon(R.drawable.ic_action_share);
		item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

//		item = menu.add(0, MENU_SETTING, 0, R.string.menu_setting);
//		item.setIcon(R.drawable.ic_action_settings);
//		item.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);

		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case MENU_SEARCH:
			// showAlertDialog(DIALOG_ABOUT);
			Log.i("test", "MENU SEARCH Click!!!");
			break;
		case MENU_SETTING:
			// showAlertDialog(DIALOG_SEARCH);
			Log.i("test", "MENU SETTING Click!!!");
			break;
		case MENU_SHARE:
			Log.i("test", "MENU SHARE Click!!!");
			share(getActivity(), "分享", "街机合集分享  http://plapk.qiniudn.com/gamebox.apk");
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	private ListView mGamesLv = null;
	private GameListAdapter mGamesAdapter = null;

	private View initView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		initData();
		View view = inflater.inflate(R.layout.fragment_games, container, false);
		mGamesLv = (ListView) view.findViewById(R.id.games_lv);
		mGamesAdapter = new GameListAdapter(getActivity());
		mGamesLv.setAdapter(mGamesAdapter);
		mGamesAdapter.updateGames(mGames);
		mGamesLv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				mGamesAdapter.onItemClick(position);
			}
		});

		return view;
	}

	ArrayList<ResGame> mGames = null;

	private void initData() {
		try {
			Dao<ResGame, Integer> gamesDao = gamesDBHelper.getGamesDao();
			mGames = new ArrayList<ResGame>();
			mGames.addAll(gamesDao.queryForAll());
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	// init DB, like http response data here!!
	private void initDB() {
		try {
			Dao<ResGame, Integer> gamesDao = gamesDBHelper.getGamesDao();
			List<ResGame> gs = gamesDao.queryForAll();
			Log.e("test", "size = " + gs.size());
			if (gs.size() == 0) {
				long t1 = System.currentTimeMillis();
				DatabaseConnection conn = gamesDao.startThreadConnection();
				Savepoint savePoint = null;
				try {
					savePoint = conn.setSavePoint(null);
					ArrayList<ResGame> games = Games.getGames();
					for (ResGame g : games) {
						gamesDao.createOrUpdate(g);
					}

				} finally {
					// commit at the end
					conn.commit(savePoint);
					gamesDao.endThreadConnection(conn);
				}
				long t2 = System.currentTimeMillis();
				Log.e("test", "dt(times) = " + (t2 - t1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onChanged(DownloadTask task) {
		if (mGamesAdapter != null)
			mGamesAdapter.update();
	}

	private void share(Context context, String title, String text) {
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.setType("text/plain");
		intent.putExtra(Intent.EXTRA_SUBJECT, title);
		intent.putExtra(Intent.EXTRA_TEXT, text);
		context.startActivity(Intent.createChooser(intent, title));
	}
}
