package com.gamebox.downloader;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;

public class HttpDownloadTask implements Runnable {

	private static final String TAG = HttpDownloadTask.class.getSimpleName();

	private DownloadTask task;
	private DownloadListener listener;
	private UIHandler handler;

	private boolean isDebug = false;

	public HttpDownloadTask(DownloadTask task, DownloadListener listener) {
		if (task == null) {
			throw new NullPointerException("DownloadTask cannot be null");
		}

		if (listener == null) {
			throw new NullPointerException("DownloadListener cannot be null");
		}
		this.task = task;
		this.listener = listener;
		this.handler = new UIHandler(Looper.getMainLooper(), this.listener);
	}

	@Override
	public void run() {
		String path = task.savepath;
		File file = new File(path);
		File tmpfile = new File(path+".temp");
		InputStream in = null;
		RandomAccessFile out = null;
		HttpURLConnection connection = null;

		try {
			long range = tmpfile.length();
			long curSize = range;
			task.totalsize = 0;
			String urlString = task.url;
			String cookies = null;
			while (true) {
				URL url = new URL(urlString);
				connection = (HttpURLConnection) url.openConnection();
				connection.setRequestProperty("Connection", "Keep-Alive");
				if (!TextUtils.isEmpty(cookies)) {
					connection.setRequestProperty("Cookie", cookies);
				}
				connection.setRequestMethod("GET");

				if (range > 0) {
					connection.setRequestProperty("Range", "bytes=" + range
							+ "-");
				}

				// http auto redirection
				// see:
				// http://www.mkyong.com/java/java-httpurlconnection-follow-redirect-example/
				boolean redirect = false;
				boolean success = false;

				// normally, 3xx is redirect
				int status = connection.getResponseCode();
				Log.i(TAG, "HTTP STATUS CODE: " + status);

				switch (status) {
				case HttpURLConnection.HTTP_OK:
				case HttpURLConnection.HTTP_PARTIAL:
					success = true;
					// 是否支持断点续传
					String accept_ranges = connection
							.getHeaderField("Accept-Ranges");
					if (!TextUtils.isEmpty(accept_ranges)
							&& accept_ranges.equalsIgnoreCase("bytes")) {
						Log.i(TAG, "Accept-Ranges: bytes");
					} else {
						range = 0;
						Log.i(TAG, "Accept-Ranges: none");
					}
					break;
				case HttpURLConnection.HTTP_MOVED_TEMP:
				case HttpURLConnection.HTTP_MOVED_PERM:
				case HttpURLConnection.HTTP_SEE_OTHER:
					redirect = true;
					// get redirect url from "location" header field
					urlString = connection.getHeaderField("Location");
					// get the cookie if need, for login
					cookies = connection.getHeaderField("Set-Cookie");
					Log.i(TAG, "Redirect Url : " + urlString);
					break;
				default:
					success = false;
					break;
				}
				if (!redirect) {
					if (!success) {
						saveTask(DownloadStatus.FAILED);
						Log.e(TAG, "Http Connection error.!!! ");
						return;
					}
					Log.i(TAG, "http connection.Ready to download...");
					break;
				}
			}
			
			// set the total file size
			task.totalsize = connection.getContentLength();
			// set the content type
			task.mimeType = connection.getContentType();

			File dir = tmpfile.getParentFile();

			if (!dir.exists() && !dir.mkdirs()) {
				saveTask(DownloadStatus.FAILED);
				Log.e(TAG, "The directory of the file can not be created!");
				return;
			}

			Log.i(TAG, "DownloadTask = " + task);

			out = new RandomAccessFile(tmpfile, "rw");
			out.seek(range);

			in = new BufferedInputStream(connection.getInputStream());

			byte[] buffer = new byte[1024];
			int nRead = 0;
			boolean isFinishDownloading = true;
			while ((nRead = in.read(buffer, 0, 1024)) > 0) {
				out.write(buffer, 0, nRead);

				curSize += nRead;

				task.currentsize = curSize;

				saveTask(DownloadStatus.RUNNING);
				if (isDebug) {
					Log.i(TAG, "task.currentsize = " + task.currentsize
							+ " ,task.totalsize = " + task.totalsize);
				}

				if (task.isCanneled() || task.isPause()) {
					isFinishDownloading = false;
					break;
				}
			}

			long len = tmpfile.length();
			if (len != 0 && len == task.totalsize) {
				tmpfile.renameTo(file);
				Log.i(TAG, "DownloadTask has been successfully downloaded.");
				saveTask(DownloadStatus.SUCCESS);
			} else if(!isFinishDownloading){
				Log.w(TAG,
						"The DownloadTask has not been completely downloaded.");
				saveTask(task.status);
			}else{
				Log.i(TAG, "DownloadTask failed to downloaded.");
				saveTask(DownloadStatus.FAILED);
			}
		} catch (MalformedURLException e) {
			saveTask(DownloadStatus.FAILED);
			e.printStackTrace();
		} catch (ProtocolException e) {
			saveTask(DownloadStatus.FAILED);
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			saveTask(DownloadStatus.FAILED);
			e.printStackTrace();
		} catch (IOException e) {
			saveTask(DownloadStatus.FAILED);
			e.printStackTrace();
		} finally {
			try {
				if (in != null) {
					in.close();
				}

				if (out != null) {
					out.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}

			if (connection != null) {
				connection.disconnect();
			}
		}
	}

	// 通知刷新UI
	private static final long EVERY_UPDATE_TIME = 1000;
	/** 上次按返回的时间 */
	private long lastUpdatetime = 0;

	private void saveTask(int status) {
		task.status = status;
		switch (task.status) {
		case DownloadStatus.CANCELED:
		case DownloadStatus.FAILED:
		case DownloadStatus.PAUSE:
		case DownloadStatus.PENDING:
		case DownloadStatus.SUCCESS:
			handler.sendMessage(handler.obtainMessage(MSG_TASK_UPDATE, task));
			break;
		case DownloadStatus.RUNNING:
			// 避免刷新过快,1s刷新一次
			long curtime = System.currentTimeMillis();
			if (curtime - lastUpdatetime > EVERY_UPDATE_TIME) {
				handler.sendMessage(handler
						.obtainMessage(MSG_TASK_UPDATE, task));
				lastUpdatetime = curtime;
//				Log.e(TAG, "update url = " + task.url + " ,lastUpdatetime = "
//						+ lastUpdatetime);
			}
		default:
			break;
		}
	}

	private static final int MSG_TASK_UPDATE = 0x1001;

	private static class UIHandler extends Handler {
		DownloadListener mListener = null;

		public UIHandler(Looper mainLooper, DownloadListener listener) {
			super(mainLooper);
			mListener = listener;
		}

		@Override
		public void handleMessage(Message msg) {
			DownloadTask task = (DownloadTask) msg.obj;
			if (task == null)
				return;
			switch (msg.what) {
			case MSG_TASK_UPDATE:
				mListener.onChanged(task);
				break;
			default:
				break;
			}
		}
	}
}
