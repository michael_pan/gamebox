package com.gamebox.games;

import java.io.Serializable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Create Table Games
 * 
 * @author michael.pan
 */
@DatabaseTable(tableName = "games")  
public class ResGame implements Serializable {
	private static final long serialVersionUID = -7026936071789656979L;
	
	@DatabaseField(generatedId = true)
	public Integer id; //自增ID
	
	@DatabaseField(canBeNull = false)
	public String gamePkgName; //包名
	
	@DatabaseField(canBeNull = false)
	public String gameName;  //游戏名
	
	@DatabaseField(canBeNull = false)
	public String gameIconUrl; //游戏ICON
	
	@DatabaseField(canBeNull = false)
	public String gameDownloadUrl; //游戏ROM下载地址
	
	@DatabaseField(canBeNull = false)
	public String gameSizeNick; // 游戏大小 例如 1.10M
	
	@DatabaseField(canBeNull = false)
	public long   gameSizeByte; // 游戏真实大小
	
	@DatabaseField(canBeNull = false)
	public String gameDesc;   // 游戏描述
	
	@DatabaseField(canBeNull = false)
	public String gameVer; // 游戏版本号 1.1.3
	
	@DatabaseField(canBeNull = false)
	public int gameVerInt; // 版本号 113000
	
	@DatabaseField(canBeNull = false)
	public String gameClassName;// 游戏分类，多个用空格或者,分隔
	
	@DatabaseField(canBeNull = false)
	public String gameDownloadCountNick;// 下载次数，给予什么就显示什么，例如：下载10w+,2013-05-10新增
	
	@Override
	public String toString() {
		return gamePkgName+","+gameName+","+gameIconUrl+","+gameDownloadUrl+","
			   +gameSizeNick+","+gameSizeByte+","+gameDesc+","+gameVer
			   +","+gameVerInt+","+gameClassName+","+gameDownloadCountNick;
	}
}
