package com.gamebox.downloader;

import java.io.IOException;
import java.sql.SQLException;

import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;

/**
 * Database helper class used to manage the creation and upgrading of your database. This class also usually provides
 * the DAOs used by the other classes.
 */
public class DownloadDBConfigUtil extends OrmLiteConfigUtil {

	public static void main(String[] args) throws SQLException, IOException {
	    final Class<?>[] classes = new Class[] { DownloadTask.class }; 
		writeConfigFile("download_config.txt", classes);
	}
}