package com.gamebox.views;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gamebox.R;

public class FeedBackFragment extends BaseFragment {

	private static FeedBackFragment mFeedBackFragment;

	public static FeedBackFragment getInstance() {
		if (mFeedBackFragment == null) {
			synchronized (FeedBackFragment.class) {
				if (mFeedBackFragment == null)
					mFeedBackFragment = new FeedBackFragment();
			}
		}
		return mFeedBackFragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return initView(inflater, container, savedInstanceState);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		menu.clear();
		MenuItem item;
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case MENU_SEARCH:
			// showAlertDialog(DIALOG_SEARCH);
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	private TextView mTV;

	private View initView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_feedback, container, false);
		mTV = (TextView) view.findViewById(R.id.title);
		mTV.setText("����");
		return view;
	}

}
