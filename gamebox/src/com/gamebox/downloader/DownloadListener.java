package com.gamebox.downloader;

public interface DownloadListener {
	public void onChanged(DownloadTask task);
}
