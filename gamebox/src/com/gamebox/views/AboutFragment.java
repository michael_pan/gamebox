package com.gamebox.views;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gamebox.R;

public class AboutFragment extends BaseFragment {

	private static AboutFragment mAboutFragment;

	public static AboutFragment getInstance() {
		if (mAboutFragment == null) {
			synchronized (AboutFragment.class) {
				if (mAboutFragment == null)
					mAboutFragment = new AboutFragment();
			}
		}
		return mAboutFragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return initView(inflater, container, savedInstanceState);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		menu.clear();
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case MENU_SEARCH:
			// showAlertDialog(DIALOG_SEARCH);
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	private TextView mTV;

	private View initView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_about, container, false);
		mTV = (TextView) view.findViewById(R.id.title);
		mTV.setText("����");
		return view;
	}

}
