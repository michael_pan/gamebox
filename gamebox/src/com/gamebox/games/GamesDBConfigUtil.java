package com.gamebox.games;

import java.io.IOException;
import java.sql.SQLException;

import com.gamebox.games.ResGame;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;

/**
 * Database helper class used to manage the creation and upgrading of your database. This class also usually provides
 * the DAOs used by the other classes.
 */
public class GamesDBConfigUtil extends OrmLiteConfigUtil {

	public static void main(String[] args) throws SQLException, IOException {
	    final Class<?>[] classes = new Class[] { ResGame.class }; 
		writeConfigFile("games_config.txt", classes);
	}
}