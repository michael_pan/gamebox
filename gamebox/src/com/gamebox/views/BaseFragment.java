package com.gamebox.views;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Menu;

public class BaseFragment extends Fragment {

	// Options Menu's IDs
	public static final int MENU_OPT = Menu.FIRST;
	public static final int MENU_SEARCH = MENU_OPT + 1; // ����
	public static final int MENU_SETTING = MENU_OPT + 2; // ����
	public static final int MENU_SHARE = MENU_OPT + 3; // ����

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}
}
