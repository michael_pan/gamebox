package com.gamebox.games;

import java.util.ArrayList;

public class Games {
	private volatile static ArrayList<ResGame> mGames;

	public static ArrayList<ResGame> getGames() {
		if (mGames == null) {
			synchronized (Games.class) {
				if (mGames == null)
					mGames = new ArrayList<ResGame>();
				initHttpData();
			}
		}
		return mGames;
	}

	private static void initHttpData() {
		ResGame g1 = new ResGame();
		g1.gameClassName = "街机，游戏";
		g1.gameDesc = "名将是一款好玩的街机游戏";
		g1.gameDownloadCountNick = "1000+";
		g1.gameDownloadUrl = "http://roms.qiniudn.com/captcomm.zip";
		g1.gameIconUrl = "http://roms.qiniudn.com/captcomm.png";
		g1.gameName = "名将";
		g1.gamePkgName = "captcomm";
		g1.gameSizeByte = 100000L;
		g1.gameSizeNick = "10.2M";
		g1.gameVer = "1.0";
		g1.gameVerInt = 10;
		
		ResGame g2 = new ResGame();
		g2.gameClassName = "街机，游戏";
		g2.gameDesc = "恐龙快打是一款好玩的街机游戏";
		g2.gameDownloadCountNick = "2000+";
		g2.gameDownloadUrl = "http://roms.qiniudn.com/dino.zip";
		g2.gameIconUrl = "http://roms.qiniudn.com/dino.png";
		g2.gameName = "恐龙快打";
		g2.gamePkgName = "dino";
		g2.gameSizeByte = 32000L;
		g2.gameSizeNick = "3.2M";
		g2.gameVer = "1.0";
		g2.gameVerInt = 10;
		
		ResGame g3 = new ResGame();
		g3.gameClassName = "街机，游戏";
		g3.gameDesc = "拳皇97是一款好玩的街机游戏";
		g3.gameDownloadCountNick = "2000+";
		g3.gameDownloadUrl = "http://roms.qiniudn.com/kof97.zip";
		g3.gameIconUrl = "http://roms.qiniudn.com/kof97.png";
		g3.gameName = "拳皇97";
		g3.gamePkgName = "kof97";
		g3.gameSizeByte = 57000L;
		g3.gameSizeNick = "5.2M";
		g3.gameVer = "1.0";
		g3.gameVerInt = 10;
		
		
		ResGame g4 = new ResGame();
		g4.gameClassName = "街机，游戏";
		g4.gameDesc = "拳皇98是一款好玩的街机游戏";
		g4.gameDownloadCountNick = "4000+";
		g4.gameDownloadUrl = "http://roms.qiniudn.com/kof98.zip";
		g4.gameIconUrl = "http://roms.qiniudn.com/kof98.png";
		g4.gameName = "拳皇98";
		g4.gamePkgName = "kof98";
		g4.gameSizeByte = 100000L;
		g4.gameSizeNick = "10.2M";
		g4.gameVer = "1.0";
		g4.gameVerInt = 10;
		
		ResGame g5 = new ResGame();
		g5.gameClassName = "街机，游戏";
		g5.gameDesc = "合金弹头是一款好玩的街机游戏";
		g5.gameDownloadCountNick = "1000+";
		g5.gameDownloadUrl = "http://roms.qiniudn.com/mslug.zip";
		g5.gameIconUrl = "http://roms.qiniudn.com/mslug.png";
		g5.gameName = "合金弹头";
		g5.gamePkgName = "mslug";
		g5.gameSizeByte = 100000L;
		g5.gameSizeNick = "10.2M";
		g5.gameVer = "1.0";
		g5.gameVerInt = 10;
		
		ResGame g6 = new ResGame();
		g6.gameClassName = "街机，游戏";
		g6.gameDesc = "街头霸王1是一款好玩的街机游戏";
		g6.gameDownloadCountNick = "1000+";
		g6.gameDownloadUrl = "http://roms.qiniudn.com/sf1.zip";
		g6.gameIconUrl = "http://roms.qiniudn.com/sf1.png";
		g6.gameName = "街头霸王1";
		g6.gamePkgName = "sf1";
		g6.gameSizeByte = 100000L;
		g6.gameSizeNick = "10.2M";
		g6.gameVer = "1.0";
		g6.gameVerInt = 10;
		
		
		ResGame g7 = new ResGame();
		g7.gameClassName = "街机，游戏";
		g7.gameDesc = "街头霸王2是一款好玩的街机游戏";
		g7.gameDownloadCountNick = "1000+";
		g7.gameDownloadUrl = "http://roms.qiniudn.com/sf2ce.zip";
		g7.gameIconUrl = "http://roms.qiniudn.com/sf2ce.png";
		g7.gameName = "街头霸王2";
		g7.gamePkgName = "sf2ce";
		g7.gameSizeByte = 100000L;
		g7.gameSizeNick = "10.2M";
		g7.gameVer = "1.0";
		g7.gameVerInt = 10;
		
		
		ResGame g8 = new ResGame();
		g8.gameClassName = "街机，游戏";
		g8.gameDesc = "忍者神龟2是一款好玩的街机游戏";
		g8.gameDownloadCountNick = "1000+";
		g8.gameDownloadUrl = "http://roms.qiniudn.com/tmnt2.zip";
		g8.gameIconUrl = "http://roms.qiniudn.com/tmnt2.png";
		g8.gameName = "忍者神龟2";
		g8.gamePkgName = "tmnt2";
		g8.gameSizeByte = 100000L;
		g8.gameSizeNick = "10.2M";
		g8.gameVer = "1.0";
		g8.gameVerInt = 10;
		
		ResGame g9 = new ResGame();
		g9.gameClassName = "街机，游戏";
		g9.gameDesc = "三国志是一款好玩的街机游戏";
		g9.gameDownloadCountNick = "1000+";
		g9.gameDownloadUrl = "http://roms.qiniudn.com/wof.zip";
		g9.gameIconUrl = "http://roms.qiniudn.com/wof.png";
		g9.gameName = "三国志";
		g9.gamePkgName = "wof";
		g9.gameSizeByte = 100000L;
		g9.gameSizeNick = "10.2M";
		g9.gameVer = "1.0";
		g9.gameVerInt = 10;
		
		mGames.add(g1);
		mGames.add(g2);
		mGames.add(g3);
		mGames.add(g4);
		mGames.add(g5);
		mGames.add(g6);
		mGames.add(g7);
		mGames.add(g8);
		mGames.add(g9);
	}
}
