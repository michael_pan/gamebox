package com.gamebox.cache;

import java.io.File;

import android.os.Environment;

public class GLOBAL {
	// 定义apk下载目录和图片下载目录
	public static final String DOWNLOADPATH = Environment
			.getExternalStorageDirectory().getAbsolutePath()
			+ "/Android/data/com.gamebox/";
	// APK下载地址
	public static final String APKDOWNLOAD = DOWNLOADPATH + File.separator
			+ "download" + File.separator + "apk";
	// 图片下载地址
	public static final String PICDOWNLOAD = DOWNLOADPATH + File.separator
			+ "download" + File.separator + "pic";
	// ROMS下载地址
	public static final String ROMDOWNLOAD = DOWNLOADPATH + File.separator
			+ "roms";

	// 从URL中解析文件名称
	public static String getFilenameFromURL(String url) {
		return url.substring(url.lastIndexOf("/") + 1, url.length());
	}

	// 保存地址
	public static String getSavePathFromURL(String url) {
		return GLOBAL.ROMDOWNLOAD + File.separator
				+ GLOBAL.getFilenameFromURL(url);
	}
}
