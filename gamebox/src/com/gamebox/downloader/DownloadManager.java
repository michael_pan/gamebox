package com.gamebox.downloader;

import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.gamebox.cache.GLOBAL;

public class DownloadManager {

	private static final String TAG = DownloadManager.class.getSimpleName();
	//
	private static DownloadManager mgr = null;
	private Context ctx;
	private DownloadListener listener = null;
	private ExecutorService executor = null;
	private static int MAX_NUM_THREAD = 3;
	// Task List
	private ArrayList<DownloadTask> tasks = null;

	// single mode
	private DownloadManager(Context ctx) {
		this.ctx = ctx;
		tasks = new ArrayList<DownloadTask>();
		PriorityThreadFactory threadFactory = new PriorityThreadFactory(
				"download", android.os.Process.THREAD_PRIORITY_BACKGROUND);
		executor = Executors.newFixedThreadPool(MAX_NUM_THREAD, threadFactory);
	}

	// Context 传入Application Context
	public static synchronized DownloadManager getInstance(Context ctx) {
		// 如果context为空
		if (ctx == null) {
			throw new NullPointerException("Context cannot be null");
		}

		if (mgr == null) {
			mgr = new DownloadManager(ctx);
		}
		return mgr;
	}

	// 注册监听
	public void registerDownloadListener(DownloadListener listener) {
		if (listener == null)
			return;
		this.listener = listener;
	}

	// // 退出
	// public void exit() {
	// // 退出线程池
	// if (executor != null) {
	// executor.shutdownNow();
	// }
	//
	// // 清空任务列表
	// if (tasks != null) {
	// tasks.clear();
	// tasks = null;
	// }
	//
	// mgr = null;
	// }

	// 添加下载任务
	public void startTask(DownloadTask task) {
		if (executor == null || task == null) {
			Log.e(TAG, "task or executor is null!!!");
			return;
		}

		if (!task.isValid()) {
			Log.e(TAG, "task is not valid!!!");
			return;
		}
		tasks.add(task);
		executor.execute(new HttpDownloadTask(task, listener));
		return;
	}

	// 获取下载任务的状态
	public int getStatus(String url) {
		int status = DownloadStatus.NONE;
		if (TextUtils.isEmpty(url))
			return status;

		// 已经下载过
		File file = new File(GLOBAL.getSavePathFromURL(url));
		if (file.exists())
			return DownloadStatus.SUCCESS;

		// 在缓存中查找下载的状态
		for (DownloadTask task : tasks) {
			if (task.url.equals(url)) {
				status = task.status;
				break;
			}
		}
		return status;
	}

	// 获取下载任务的进度(百分比)
	public int getPrecent(String url) {
		int precent = 0;
		if (TextUtils.isEmpty(url))
			return precent;
		
		// 计算下载百分比
		for (DownloadTask task : tasks) {
			if (task.url.equals(url) && task.totalsize != 0) {
				precent = (int) (task.currentsize * 100 / task.totalsize);
				break;
			}
		}
		return precent;
	}

}
