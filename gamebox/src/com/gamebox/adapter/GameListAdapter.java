package com.gamebox.adapter;

import java.io.File;
import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.gamebox.R;
import com.gamebox.cache.GLOBAL;
import com.gamebox.cache.PicMgr;
import com.gamebox.downloader.DownloadManager;
import com.gamebox.downloader.DownloadStatus;
import com.gamebox.downloader.DownloadTask;
import com.gamebox.games.ResGame;
import com.gamebox.utils.UIUtils;
import com.seleuco.mame4all.MAME4all;

/**
 * @ClassName: GameListAdapter
 * @Description: 应用列表Adapter
 */

public class GameListAdapter extends BaseAdapter {
	private static final String TAG = "GameListAdapter";
	private ArrayList<ResGame> games;
	private Context ctx;
	private DownloadManager dm;

	public GameListAdapter(Context ctx) {
		this.ctx = ctx;
		this.games = new ArrayList<ResGame>();
		this.dm = DownloadManager.getInstance(ctx);
	}

	@Override
	public int getCount() {
		return games.size();
	}

	@Override
	public Object getItem(int position) {
		return games.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public void updateGames(ArrayList<ResGame> games) {
		this.games = games;
		notifyDataSetChanged();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = LayoutInflater.from(ctx).inflate(
					R.layout.game_list_item, null);
			holder = new ViewHolder();
			holder.icon = (ImageView) convertView.findViewById(R.id.gameicon);
			holder.name = (TextView) convertView.findViewById(R.id.gamename);
			holder.size = (TextView) convertView.findViewById(R.id.gamesize);
			holder.count = (TextView) convertView.findViewById(R.id.gamecount);
			holder.content = (TextView) convertView
					.findViewById(R.id.gamecontent);
			holder.download = (Button) convertView
					.findViewById(R.id.gamedownload);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		// 显示其他属性
		ResGame game = games.get(position);
		holder.name.setText(String.valueOf(position + 1) + "." + game.gameName);
		holder.size.setText("大小：" + game.gameSizeNick);
		holder.count.setText("下载 : " + game.gameDownloadCountNick);
		holder.content.setText(game.gameDesc);
		// 显示图标
		PicMgr.getInstance(ctx).loadBitmap(holder.icon, game.gameIconUrl);
		// 显示下载按钮
		holder.download.setTag(game);
		holder.download.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				ResGame game = (ResGame) v.getTag();
				int status = dm.getStatus(game.gameDownloadUrl);
				if (status == DownloadStatus.NONE) {
					DownloadTask task = new DownloadTask();
					task.name = GLOBAL.getFilenameFromURL(game.gameDownloadUrl);
					task.savepath = GLOBAL
							.getSavePathFromURL(game.gameDownloadUrl);
					task.desc = game.gameDesc;
					task.title = game.gameName;
					task.url = game.gameDownloadUrl;
					dm.startTask(task);
				} else if (status == DownloadStatus.SUCCESS) {
					Intent intent = new Intent(ctx, MAME4all.class);
					intent.setAction(Intent.ACTION_VIEW);
					intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					File path = new File(GLOBAL
							.getSavePathFromURL(game.gameDownloadUrl));
					intent.setData(Uri.fromFile(path));
					ctx.startActivity(intent);
					UIUtils.createShortcutIntent(ctx, game.gameName,
							game.gameDownloadUrl, game.gameIconUrl);
				}
			}
		});
		int status = dm.getStatus(game.gameDownloadUrl);
		if (status == DownloadStatus.NONE) {
			holder.download.setText(R.string.btn_install);
		} else if (status == DownloadStatus.SUCCESS) {
			holder.download.setText(R.string.btn_launch);
		} else if (status == DownloadStatus.RUNNING
				|| status == DownloadStatus.PENDING) {
			holder.download.setText(dm.getPrecent(game.gameDownloadUrl) + "%");
		} else {
			holder.download.setText(R.string.btn_install);
		}
		return convertView;
	}

	static class ViewHolder {
		ImageView icon;
		TextView name;
		TextView size;
		TextView count;
		TextView content;
		Button download;
	}

	public void update() {
		notifyDataSetChanged();
	}

	public void onItemClick(int position) {
		if (position < 0 || position >= getCount())
			return;
		Log.e("test", "position = " + position);
		ResGame game = games.get(position);
		int status = dm.getStatus(game.gameDownloadUrl);
		if (status == DownloadStatus.SUCCESS) {
			Intent intent = new Intent(ctx, MAME4all.class);
			intent.setAction(Intent.ACTION_VIEW);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			File path = new File(
					GLOBAL.getSavePathFromURL(game.gameDownloadUrl));
			intent.setData(Uri.fromFile(path));
			ctx.startActivity(intent);
			UIUtils.createShortcutIntent(ctx, game.gameName,
					game.gameDownloadUrl, game.gameIconUrl);
		}
	}
}
